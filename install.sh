#!/usr/bin/env bash

# Fail on first error
set -e

# Copy release executable
cp target/release/githooks /bin/githooks

# Create service
cp systemd/githooks.service /etc/systemd/system/githooks.service
