use std::process::{Command, Output};
use std::io::Result;

pub struct Action {
    cwd: String,
    script: Vec<String>,
}

impl Action {
    pub fn new(cwd: String, script: Vec<String>) -> Action {
        Action {
            cwd: cwd,
            script: script,
        }
    }

    pub fn execute(&self) -> Result<Output> {
        // Concatenate the script to a single line
        let script = self.script.join(";\\\n");

        // The shell is assumed to be bash
        Command::new("bash")
            .current_dir(&self.cwd)
            .args(&["-c", &script]).output()
    }
}

