pub type Repo = String;
pub type Token = String;
pub type Event = String;

pub struct HookEvent {
    pub token: Token,
    pub event: Event,
    pub repo: Repo,
}

macro_rules! unwrap_or_return {
    ($e: expr) => {
        match $e {
            Ok(v) => v,
            Err(e) => { return Err(e); }
        }
    }
}

pub trait LoadHookEvent {
    type LoadError;

    fn load_token(&self) -> Result<Token, Self::LoadError>;
    fn load_event(&self) -> Result<Event, Self::LoadError>;
    fn load_repo(&self) -> Result<Repo, Self::LoadError>;

    fn load_hook_event(&self) -> Result<HookEvent, Self::LoadError> {
        let token = unwrap_or_return!(self.load_token());
        let event = unwrap_or_return!(self.load_event());
        let repo = unwrap_or_return!(self.load_repo());

        let he = HookEvent {
            token: token,
            event: event,
            repo: repo
        };

        Ok(he)
    }
}
