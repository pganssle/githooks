extern crate hyper;
extern crate chrono;

use hyper::server::Request;
use hyper::header::UserAgent;
use chrono::Utc;

pub fn log_request(req: &Request) { 
    let date = Utc::now(); 
    let dflt_user_agent = UserAgent::new("Unknown");
    let user_agent = req.headers().get::<UserAgent>()
        .unwrap_or(&dflt_user_agent); 

    println!("{} \"{} {} {}\" \"{}\"\nHeaders: {}", 
             date,
             req.method(), 
             req.uri(), 
             req.version(),
             user_agent,
             req.headers()
    );
}