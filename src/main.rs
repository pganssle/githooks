extern crate hyper;
extern crate clap;
extern crate futures;
extern crate chrono;
extern crate yaml_rust;
extern crate serde_json;

mod actions;
mod config;
mod logging;
mod hook_events;
mod service;
mod yaml_handler;

use std::sync::Arc;

use clap::{Arg, App, ArgMatches};
use hyper::server::Http;

use service::GitHooks;

use yaml_rust::Yaml;
use yaml_handler::YamlLoadable;

struct CliConfig {
    /* Configuration loaded from the CLI or a config file */
    hostname: Option<String>,
    port: Option<u32>,
    token_path: Option<String>,
    events_path: Option<String>,
}

trait Merge {
    fn merge(&self, other: &Self) -> Self;
}


impl Merge for CliConfig {
    fn merge(&self, other: &Self) -> Self {
        /* Creates a new CliConfig by overriding the fields in self
           with those in other that are not None */
        
        macro_rules! merge_field {
            ($fieldname:ident) => (
                match other.$fieldname {
                    Some(ref field) => Some(field.to_owned()),
                    None => (self.$fieldname).to_owned()
                }
            )
        };

        macro_rules! merged_from_fields {
            ($( $fieldname:ident ),*) => (
                Self {
                    $($fieldname: merge_field!($fieldname)),*
                }
            )
        }

        merged_from_fields!(hostname, port, token_path, events_path)
    }
}


impl<'a> std::convert::From<ArgMatches<'a>> for CliConfig {
    fn from(matches: ArgMatches) -> Self {
        let hostname = matches.value_of("hostname").map(str::to_string);
        let token_path = matches.value_of("tokens").map(str::to_string);
        let events_path = matches.value_of("events").map(str::to_string);

        let port = matches.value_of("port").map(|x| str::parse::<u32>(x).unwrap());

        Self {
            hostname: hostname,
            port: port,
            token_path: token_path,
            events_path: events_path,
        }
    }
}

impl yaml_handler::YamlLoadable for CliConfig {
    fn from_yaml(node: &Yaml) -> Self {
        let root = node.as_hash().unwrap();
        let yaml_to_str = |x: &Yaml| {
            match x.as_str() {
                Some(s) => Some(s.to_string()),
                None => None
            }
        };

        let yaml_to_u32 = |x: &Yaml| {
            match x.as_i64() {
                Some(i) => Some(i as u32),
                None => None
            }
        };

        macro_rules! get_val {
            ($key:expr, $func:expr) => {
                root.get(&Yaml::from_str($key)).and_then($func);
            }
        };

        let hostname = get_val!("hostname", &yaml_to_str);
        let port = get_val!("port", yaml_to_u32);
        let token_path = get_val!("tokens", &yaml_to_str);
        let events_path = get_val!("events", &yaml_to_str);

        Self {
            hostname: hostname,
            port: port,
            token_path: token_path,
            events_path: events_path
        }
    }
}

impl CliConfig {
    fn valid(&self) -> bool {
        {
            self.hostname.is_some() &&
            self.port.is_some() &&
            self.token_path.is_some() &&
            self.events_path.is_some()
        }
    }
}

fn main() {
    // Load metadata from Cargo
    const PKG_NAME: &'static str = env!("CARGO_PKG_NAME");
    const VERSION: &'static str = env!("CARGO_PKG_VERSION");
    const DESC: &'static str = env!("CARGO_PKG_DESCRIPTION");

    // Set up the CLI
    let matches = App::new(PKG_NAME)
        .version(VERSION)
        .about(DESC)
        .arg(Arg::with_name("config")
            .short("c")
            .required(false)
            .takes_value(true)
            .help("A config .yaml file for default configurations \
                  (overridden by command line options)"))
        .arg(Arg::with_name("hostname")
                .short("H")
                .required(false)
                .takes_value(true)
                .help("The hostname to listen on (default: 127.0.0.1)"))
        .arg(Arg::with_name("port")
                 .short("p")
                 .required(false)
                 .takes_value(true)
                 .help("The port to listen on (default: 9191"))
        .arg(Arg::with_name("tokens")
                .short("t")
                .required(false)
                .takes_value(true)
                .help("A YAML file mapping repositories to tokens"))
        .arg(Arg::with_name("events")
            .short("e")
            .takes_value(true)
            .help("A YAML file mapping events on repos to scripts to run"))
        .get_matches();


    let mut default_config = CliConfig {
        hostname: Some("127.0.0.1".to_string()),
        port: Some(9191 as u32),
        token_path: Some("/etc/githooks/tokens.yml".to_string()),
        events_path: Some("/etc/githooks/events.yml".to_string())
    };

    // Read the config file
    match matches.value_of("config") {
        Some(fpath) => {
            default_config = default_config.merge(&CliConfig::from_yaml_file(fpath));
        },
        None => {}
    }
 
    // Parse the CLI options
    let config = default_config.merge(&CliConfig::from(matches));
    
    assert!(config.valid());
    let hostname = config.hostname.unwrap();
    let port = config.port.unwrap();
    let events_path = config.events_path.unwrap();
    let token_path = config.token_path.unwrap();

    // Load the event and hook configuration from their YAML files
    let hook_config = config::HookEventConfig::new(&events_path, &token_path).unwrap();
    let token_headers = service::get_default_token_headers();
    
    let hook_config_ref = Arc::new(hook_config);
    let token_headers_ref = Arc::new(token_headers);

    // Set up server
    let listen_url = format!("{}:{}", hostname, port);
    println!("Listening on {}", listen_url);

    let addr = listen_url.parse().unwrap();

    let server = Http::new().bind(&addr,
        move || Ok(GitHooks::new((&hook_config_ref).clone(),
                            (&token_headers_ref).clone()))).unwrap();

    // Run the server
    server.run().unwrap();
}
