use futures;
use hyper;
use serde_json;

use futures::future::Future;
use futures::Stream;
use hyper::header::ContentLength;
use hyper::server::{Request, Response, Service};

use std::collections::HashSet;
use std::str::FromStr;
use std::sync::Arc;

use config;
use logging::log_request;
use hook_events::{Token, Event, Repo, LoadHookEvent};

const RESP_OK: &'static str = "OK";

pub const GITHUB_TOKEN_HEADER: &'static str = "X-Github-Token";
pub const GITLAB_TOKEN_HEADER: &'static str = "X-Gitlab-Token";

pub fn get_default_token_headers() -> HashSet<String> {
    let mut headers = HashSet::new();
    headers.insert(String::from_str(GITHUB_TOKEN_HEADER).unwrap());
    headers.insert(String::from_str(GITLAB_TOKEN_HEADER).unwrap());
    
    headers    
}

pub struct GitHooks {
    hook_config: Arc<config::HookEventConfig>,
    token_headers: Arc<HashSet<String>>
}

impl GitHooks {
    pub fn new(hook_config: Arc<config::HookEventConfig>,
               token_headers: Arc<HashSet<String>>) -> GitHooks {
        GitHooks {
            hook_config: hook_config,
            token_headers: token_headers
        }
    }
}

impl Service for GitHooks {
    // boilerplate hooking up hyper's server types
    type Request = Request;
    type Response = Response;
    type Error = hyper::Error;
    
    // The future representing the eventual Response your call will
    // resolve to. This can change to whatever Future you need.
    type Future = Box<Future<Item=Self::Response, Error=Self::Error>>;


    fn call(&self, _req: Request) -> Self::Future {
        log_request(&_req);      // Log the request
    
        let resp = Response::new()
            .with_header(ContentLength(RESP_OK.len() as u64))
            .with_body(RESP_OK);
 
        // Respond with OK
        let rh = RequestHandler::new(_req,
                                     self.token_headers.clone(),
                                     self.hook_config.clone());

        let fut_resp = rh.handle().and_then(|_| {
            futures::future::ok(resp)
        });

        Box::new(fut_resp)
    }
}

struct RequestHandler {
    token_headers: Arc<HashSet<String>>,
    config: Arc<config::HookEventConfig>,
    headers: hyper::header::Headers,
    body: Option<hyper::Body>,
    body_cache: Option<serde_json::Value>,
}

impl RequestHandler {
    fn new(req: Request,
           token_headers: Arc<HashSet<String>>,
           config: Arc<config::HookEventConfig>) -> Self {
        let (_meth, _uri, _http_ver, headers, body) = req.deconstruct();
        RequestHandler {
            token_headers: token_headers,
            config: config,
            headers: headers,
            body: Some(body),
            body_cache: None
        }
    }

    fn get_body(&self) -> Result<&serde_json::Value, &str> {
        match self.body_cache {
            Some(ref b) => Ok(b),
            None => Err("Cached body not found.")
        }
    }

    fn cache_body(&mut self, body_val: &str) -> &Self {
        self.body_cache = match serde_json::from_str(body_val) {
            Ok(v) => Some(v),
            Err(e) => {
                // TODO: Logging framework
                println!("Failed to parse request:\n{}\n\nError: {}",
                         body_val, e);
                None
            }
        };

        self
    }

    fn handle(mut self) -> Box<Future<Item=(),Error=hyper::Error>>{
        let body = self.body.take().unwrap();
        let result = body
            .fold(Vec::new(), |mut acc, chunk| {
                // Gather the entire body
                acc.extend_from_slice(&*chunk);
                futures::future::ok::<_, hyper::Error>(acc)
            })
            .and_then(move |chunk| {
                let v = chunk.to_vec();
                let s = String::from_utf8(v).unwrap_or("No body".to_owned());
                self.cache_body(&s);
                let hook_event = self.load_hook_event();
                match hook_event {
                    Ok(h) => {
                        self.config.execute_event(&h);
                        Ok::<(), hyper::Error>(())
                    },
                    Err(e) => {
                        println!("Error: {}", e);
                        Ok::<(), hyper::Error>(())
                    }
                }
            });

        Box::new(result)
    }
}

macro_rules! load_json_value {
    ($val:expr, $key:expr) => ($val.get($key));
    ($val:expr, $key:expr, $($keys:expr),+) => (
        match load_json_value!($val, $key) {
            Some(v) => load_json_value!(v, $($keys),+),
            None => None
        }
    )
}

macro_rules! load_json_str {
    ($val:expr, $($keys:expr),+) => (
        match load_json_value!($val, $($keys),+) {
            Some(v) => {
                // For some reason serde_json .as_x returns Option not Result
                match v.as_str() {
                    Some(s) => Ok(s),
                    None => Err("Value is None, not String")
                }
            },
            None => Err("Keys not found in JSON")
        }
    )
}

use std;
struct LoadError {
    msg: String
}

impl std::convert::From<std::string::ParseError> for LoadError {
    fn from(error: std::string::ParseError) -> Self {
        LoadError { msg: format!("{}", error) }
    }
}

impl<'a> std::convert::From<&'a str> for LoadError {
    fn from(s: &'a str) -> Self {
        LoadError { msg: s.to_owned() }
    }
}

impl std::fmt::Display for LoadError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        write!(f, "{}", self.msg)
    }
}

impl LoadHookEvent for RequestHandler {
    type LoadError = LoadError;

    fn load_token(&self) -> Result<Token, Self::LoadError> {
        for header in self.headers.iter() {
            if self.token_headers.contains(header.name()) {
                println!("Token found: {}", header.value_string());
                return Ok(header.value_string());
            }
        }

        Err(LoadError::from("No token headers found"))
    }

    fn load_event(&self) -> Result<Event, Self::LoadError> {
        let body = self.get_body()?;

        match load_json_str!(body, "object_kind") {
            Ok(s) => Ok(Event::from_str(s)?),
            Err(e) => Err(LoadError::from(e))
        }
    }

    fn load_repo(&self) -> Result<Repo, Self::LoadError> {
        let body = self.get_body()?;

        // "repository" is deprecated in favor of "project"
        match load_json_str!(body, "project", "name") {
            Ok(s) => Ok(Repo::from_str(s)?),
            Err(_e) => {
                // Fall back to trying repository
                match load_json_str!(body, "repository", "name") {
                    Ok(s) => Ok(Repo::from_str(s)?),
                    Err(e) => Err(LoadError::from(e))
                }
            }
        }
    }
}