use std;
use std::fs::File;
use std::io::Read;

use yaml_rust::Yaml;

use yaml_rust::YamlLoader as yr_YamlLoader;

fn read_file(fpath: &str) -> Result<String, String> {
    let mut file = match File::open(fpath) {
        Ok(f) => f,
        Err(e) => { return Err(format!("Unable to open file ({})", e)); }
    };

    let mut contents = String::new();
    match file.read_to_string(&mut contents) {
        Ok(s) => s,
        Err(e) => { return Err(format!("Failed to read file ({})", e)); }
    };

    Ok(contents)
}

pub fn load_yaml_file(fpath: &str) -> Result<Vec<Yaml>, String> {
    let contents = match read_file(fpath) {
        Ok(c) => c,
        Err(e) => {
            let err_str = format!("Error reading {}: {}", fpath, &e);
            return Err(err_str);
        }
    };

    match yr_YamlLoader::load_from_str(&contents) {
        Ok(y) => Ok(y),
        Err(e) => {
            let err_str = format!("Error loading YAML from {}: {}", fpath, e);
            Err(err_str)
        }
    }
}

pub trait YamlLoadable
where Self: std::marker::Sized {

    fn from_yaml(node: &Yaml) -> Self;

    fn from_yaml_str(contents: &str) -> Self {
        let yaml = yr_YamlLoader::load_from_str(contents).unwrap();

        Self::from_yaml(&yaml[0])
    }

    fn from_yaml_file(fpath: &str) -> Self {
        let contents = match read_file(fpath) {
            Ok(c) => c,
            Err(s) => panic!("{}", s)
        };

        Self::from_yaml_str(&contents)
    }
}
